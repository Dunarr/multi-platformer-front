# VIA - Multiplayer platformer - Front
The front of the game 

Require node >= 16

## Run the project
install dependencies:
```shell
npm install
```

configure env variables in `.env.local`
```dotenv
VITE_SERVER_URL="http://127.0.0.1:3000" # The url of the server (let the default value if using the official server on local)
```

start dev server
```shell
npm run dev
```