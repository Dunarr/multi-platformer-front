import './style.css'
import {getSprites, loadSprites} from "./spriteManager.js";
import characters from "./assets/characters.png"
import sheet from "./assets/sheet.png"
import sky from "./assets/sky.png"
import swoosh from "./assets/swoosh.png"
import Block from "./Entity/Block.js";
import Player from "./Entity/Player.js";

import socketIO from "socket.io-client"
import Pnj from "./Entity/Pnj.js";

const socket = socketIO(import.meta.env.VITE_SERVER_URL)





const canvas = document.getElementById("app")

const ctx = canvas.getContext("2d")
ctx.imageSmoothingEnabled = false;

const entities = []
const otherPlayers = {

}
let player = null

loadSprites({characters, sheet, sky, swoosh}).then(sprites => {


    entities.push(new Block(0,0))
    entities.push(new Block(1,0))
    entities.push(new Block(2,0))
    entities.push(new Block(9,0))
    entities.push(new Block(10,0))
    entities.push(new Block(5,1))
    entities.push(new Block(6,2))
    entities.push(new Block(7,3))
    entities.push(new Block(11,0))
    entities.push(new Block(12,0))
    entities.push(new Block(11,4))
    entities.push(new Block(12,4))
    entities.push(new Block(13,4))
    entities.push(new Block(14,4))
    entities.push(new Block(16,6))
    entities.push(new Block(17,6))
    entities.push(new Block(19,6))
    entities.push(new Block(20,6))
    entities.push(new Block(20,5))
    entities.push(new Block(20,4))
    entities.push(new Block(20,4))
    entities.push(new Block(20,9))
    entities.push(new Block(20,10))
    entities.push(new Block(20,11))
    entities.push(new Block(20,12))
    entities.push(new Block(20,13))
    entities.push(new Block(21,4))
    entities.push(new Block(22,4))
    entities.push(new Block(23,4))
    entities.push(new Block(24,4))
    entities.push(new Block(25,4))
    player = new Player(0, 1)
    entities.push(player)

    gameLoop()

})

socket.on("player-updated", e => {
if(!otherPlayers[e.id]){
    otherPlayers[e.id] = new Pnj()
    entities.push(otherPlayers[e.id])
}
    otherPlayers[e.id].positionX = e.x
    otherPlayers[e.id].positionY = e.y

})

function gameLoop(){
    entities.forEach(entity => {
        entity.update(entities)
    })

    socket.emit("update-player",{
        x: player.positionX,
        y: player.positionY
    })

    ctx.drawImage(getSprites().sky, 0,0,1920,1080)
    entities.forEach(entity => {
        entity.display(ctx)
    })
    requestAnimationFrame(gameLoop)
}