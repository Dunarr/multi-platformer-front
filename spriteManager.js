export function loadSprites(sources){
    return Promise.all(
        Object.entries(sources).map(([name,source]) => {
            return new Promise(resolve => {
                const imgTag = document.createElement("img")
                imgTag.src = source

                imgTag.addEventListener("load",()=> {
                    resolve([name, imgTag])
                })
            })
        })
    ).then(result => {
        sprites = Object.fromEntries(result);
        return sprites
    })
}

let sprites

export function getSprites(){
    return sprites
}