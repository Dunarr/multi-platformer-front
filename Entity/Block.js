import Entity from "./Entity.js";
import {getSprites} from "../spriteManager.js";

export default class Block extends Entity{

    constructor(x, y) {
        super();
        this.sprite = getSprites().sheet;
        this.height=64
        this.width=64
        this.solid=true
        this.positionX = x*64
        this.positionY = y*64
        this.spritePosition = {
            x:128,y:0,w:16,h:16
        }
    }
}