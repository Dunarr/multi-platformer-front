import Entity from "./Entity.js";
import {getSprites} from "../spriteManager.js";

export default class Pnj extends Entity{

    constructor() {
        super();
        this.sprite = getSprites().characters
        this.height = 64
        this.width = 42
        this.solid = false
        this.positionX = 0
        this.positionY = 0
        this.spritePosition = {
            x:8,y:8,w:16,h:24
        }
    }
}