import Entity from "./Entity.js";
import {getSprites} from "../spriteManager.js";

export default class Player extends Entity{

    lastUpdate
    speedX
    speedY
    onFloor = true
    up = false
    left = false
    right = false

    constructor(x, y) {
        super();
        this.speedX = 0
        this.speedY = 0
        this.lastUpdate = Date.now()
        this.sprite = getSprites().characters
        this.height = 64
        this.width = 42
        this.solid = false
        this.positionX = x*64
        this.positionY = y*64
        this.spritePosition = {
            x:8,y:40,w:16,h:24
        }
        document.addEventListener("keydown",(event)=>{
            console.log(event.key)
            switch (event.key){
                case "ArrowUp":
                case "z":
                case " ":
                    this.up = true
                    break;
                case "ArrowLeft":
                case "q":
                    this.left = true
                    break;
                case "ArrowRight":
                case "d":
                    this.right = true
                    break;
            }
        })
        document.addEventListener("keyup",event=>{
            switch (event.key) {
                case "ArrowUp":
                case "z":
                case " ":
                    this.up = false
                    break;
                case "ArrowLeft":
                case "q":
                    this.left = false
                    break;
                case "ArrowRight":
                case "d":
                    this.right = false
                    break;
            }
        })
    }

    update(entities) {
        const now = Date.now()
        const delta = now - this.lastUpdate
        this.lastUpdate = now
        if(navigator.getGamepads().length > 0){
            const gamepad = navigator.getGamepads()[0]
            this.speedX = gamepad.axes[0]
            if(this.speedX > -0.1 && this.speedX < 0.1){
                this.speedX = 0
            }
            if(gamepad.buttons[0].pressed && this.onFloor){
                this.speedY = 1
            }
        } else {
            if(this.left) {
                this.speedX = -1
            }
            if(this.right) {
                this.speedX = 1
            }
            if(this.right && this.left) {
                this.speedX = 0
            }
            if(!this.right && !this.left) {
                this.speedX = 0
            }
            if(this.up && this.onFloor){
                this.speedY = 1
            }

        }

        this.speedY -= 0.003 * delta

        this.onFloor = false
        entities.filter(({solid})=>solid).find(entitiy=>{
            if (this.positionX + this.width > entitiy.positionX
                && this.positionX < entitiy.positionX + entitiy.width){
                if(this.positionY <= entitiy.positionY + entitiy.height){
                    if(this.positionY >= entitiy.positionY + entitiy.height - 20){
                        this.onFloor = true
                        this.positionY = entitiy.positionY + entitiy.height
                        this.speedY = Math.max(this.speedY, 0)
                    }
                }
                if(this.positionY < entitiy.positionY + entitiy.height && this.positionY + this.height > entitiy.positionY){
                   if(entitiy.positionX + 0.5*entitiy.width > this.positionX + 0.5*this.width) {
                       this.speedX = Math.min(this.speedX, 0)
                   } else {
                       this.speedX = Math.max(this.speedX, 0)
                   }
                }
            }
        })
        this.speedY = Math.max(-1, this.speedY)
        this.positionX+=0.6 * delta * this.speedX
        this.positionY+= delta * this.speedY
    }
}