export default class Entity {
    positionX
    positionY
    width
    height
    solid
    sprite
    spritePosition
    update(){

    }
    display(ctx){
        ctx.drawImage(
            this.sprite,
            this.spritePosition.x,
            this.spritePosition.y,
            this.spritePosition.w,
            this.spritePosition.h,
            this.positionX,
            1080 - this.positionY - this.height,
            this.width,
            this.height
        )
    }
}